require 'yaml'
require 'psych/handlers/recorder'
@esmtp = true
Dir.foreach('.') do |item|
  next if item.start_with? "."
  if item.end_with? ".yml" 
    puts "Validating file #{item}"
    
    begin
      recorder = Psych::Handlers::Recorder.new
      parser = Psych::Parser.new recorder
      parser.parse(File.open(item))
    rescue Psych::SyntaxError => ex
      puts "Validation failed: #{ex.message}"
      @esmtp = false
    end
  end
end

if (@esmtp == false)
    raise "Found validation errors."
end
