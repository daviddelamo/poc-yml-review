#!/bin/bash

function parse_yaml {
   declare -a keys
   keys=()
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\):|\1|" \
        -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   ERROR=awk -F$fs '
   {
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])(".")}
		 
		 clave =  vn $2
		 if (clave in valores) {
			printf("Clave %s repetida\n",$0);
			print "ERROR=1"
		 } else {
		  valores[clave]="1";
		 }
		  
      }
   }'
   echo $ERROR
}

regex="=|:[a-zA-Z]"
regex_onlytabs="\t"
failed=0
for file in `find . -type f \( -name "*.yml" ! -iname ".*" \)`
do
	echo "Validating $file"
	if [[ $file =~ $regex_onlytabs ]]; then
		echo "One or more lines contains tabulator characters, please use only spaces."
	fi
	let line_number=0
	while IFS='' read -r line || [[ -n "$line" ]]; do
		let line_number++
		if [[ $line =~ $regex ]]; then
			echo "Found invalid characters in line $line_number of file $file"
			let failed++
			i=1
			n=${#BASH_REMATCH[*]}
			while [[ $i -lt $n ]]
			do
				if [[ ${BASH_REMATCH[$i]} = "t" ]]; then
					echo "One or more lines contains tabulator characters, please use only spaces."
				else
					echo "Found invalid character: '${BASH_REMATCH[$i]}'"		
				fi
				let i++
			done
		fi
	done <"$file"
	#parse_yaml $file
done

exit $failed
